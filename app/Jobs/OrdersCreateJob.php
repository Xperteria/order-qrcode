<?php namespace App\Jobs;

use App\Models\Shop;
use App\Models\ShopifyWebhook;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Traits\HelperTrait;

class OrdersCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,HelperTrait;

    /**
     * Shop's myshopify domain
     *
     * @var string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string $shopDomain The shop's myshopify domain
     * @param object $data    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::Info("=============== Order Create Webhook ==================");
        $shop = Shop::where('shopify_domain',$this->shopDomain)->first();
        $order = json_encode($this->data);
        $data = json_decode($order);


        $entity = ShopifyWebhook::updateOrCreate(
            ['shopify_id' => $data->id, 'topic' => 'orders/create', 'shop_id' => $shop->id],
            [   'shopify_id' => $data->id,
                'topic' => 'orders/create',
                'shop_id' => $shop->id,
                'data' => $order,
                'is_executed' => 0,
                'order_number' => "#".$data->order_number,
                'customer' => @$data->customer->first_name." ".@$data->customer->last_name,
                'total' => $data->total_price,
                'payment_status' => $data->financial_status
            ]
        );
        $this->createOrder($entity->id, $shop);
    }
}
