<?php namespace App\Traits;

use App\Models\ShopifyWebhook;
use CodeItNow\BarcodeBundle\Utils\QrCode;

trait HelperTrait{

    public function createOrder($entity_id, $shop){

            $entity = ShopifyWebhook::find($entity_id);
            $data = json_decode($entity->data,1);

            $parameter = [];
            foreach ($data['line_items'] as $key => $val) {
                $lineitem = [];
                $unique_id = time();
                $lineitem['properties'][0]['name'] = 'unique_id';
                $lineitem['properties'][0]['value'] = $unique_id;

                $page_url = config('app.page_url')."/page/".$shop->id."/".$val['product_id']."/".$val['variant_id'];
                $lineitem['properties'][1]['name'] = 'page_url';
                $lineitem['properties'][1]['value'] = $page_url;

                $barcode = $this->barcode($unique_id, $val['product_id'], $val['variant_id'], $page_url);
                $lineitem['properties'][2]['name'] = 'barcode_url';
                $lineitem['properties'][2]['value'] = $barcode;

                $lineitem['properties'][3]['name'] = 'line_item_id';
                $lineitem['properties'][3]['value'] = $val['id'];

                $parameter['namespace'] = 'orders';
                $parameter['key'] = 'line_items_'.$val['id'];
                $parameter['value_type'] = 'json_string';
                $parameter['value'] = json_encode($lineitem);
                $metafield = [];
                $metafield['metafield'] = $parameter;
                $r = $shop->api()->rest('POST',"/admin/api/orders/{$data['id']}/metafields.json",$metafield);

            }
            $entity->is_executed = 1;
            $entity->save();

    }

    public function barcode($uniqid, $product_id, $variant_id, $page_url) {
        $qrCode = new QrCode();
        $qrCode
            ->setText($page_url)
            ->setSize(150)
            ->setPadding(10)
            ->setErrorCorrection('high')
            ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
            ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
            ->setLabelFontSize(16)
            ->setImageType(QrCode::IMAGE_TYPE_PNG);

        $image = $qrCode->generate();
        $image = str_replace(' ', '+', $image);
        $imageName = $uniqid.'.'.'png';
        $dir = "qrcode";
        if (!\Storage::disk('public')->exists($dir)) {
            \Storage::disk('public')->makeDirectory($dir);
        }

        \File::put(storage_path('app/public/'.$dir). '/' . $imageName, base64_decode($image));
        return asset("/storage/".$dir . '/' . $imageName);
    }
}



