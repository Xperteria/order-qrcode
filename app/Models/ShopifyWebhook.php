<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopifyWebhook extends Model
{
    protected $fillable = ["shopify_id", "topic", "shop_id", "data", "created_at", "updated_at","id", "is_executed", 'order_number', 'customer', 'total', 'payment_status'];

    public function scopeShop($query, $shop_id) {
        return $query->where('shop_id', $shop_id);
    }

    public function shop(){
        return $this->belongsTo(Shop::class);
    }
}
