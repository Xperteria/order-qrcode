<?php

namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;
use App\Models\Shop;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function info(Request $request, Shop $shop, $product,$variant){
        $url = $request->url();
        if($request->filled('start')){
            return $this->start( $request,  $shop, $product,$variant);
        }
        return view('pages.enter-to-journey',compact('url'));
    }

    public function start(Request $request, Shop $shop, $product ,$variant){
        $url = $request->url();
        $response = $this->api($shop,$variant);
        $date = "";
        foreach (@$response->product->tags as $kry => $val){
            if(\Str::contains($val, ['mfr'])){
                $val = str_replace("-"," ",$val);
                $val = str_replace("mfr","",$val);
                $date = strtoupper($val);
                break;
            }
        }
        return view('pages.index', compact('response','date'));
    }


    public function api(Shop $shop,$variant){
        $gql = <<< EOF
            query MyQuery {
              productVariant(id: "gid://shopify/ProductVariant/{$variant}") {
                title
                selectedOptions {
                  name
                  value
                }
                image {
                  originalSrc
                }
                sku
                product {
                  title
                  tags
                }
              }
            }
EOF;

        $gqlResponse = $shop->api()->graph($gql);
        $response = [];

        if(!$gqlResponse->errors) {
            $response = $gqlResponse->body->productVariant;
        }
        return $response;
    }
}
