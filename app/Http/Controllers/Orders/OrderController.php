<?php

namespace App\Http\Controllers\Orders;

use App\Http\Controllers\Controller;
use App\Models\ShopifyWebhook;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shop = \ShopifyApp::shop();
        $orders = ShopifyWebhook::query()->shop($shop->id)->select(["shopify_id", "shop_id","id" , 'order_number', 'customer', 'total', 'payment_status', 'created_at'])->paginate();
        return response($orders,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shop = \ShopifyApp::shop();
        $order = ShopifyWebhook::find($id);
        $data = json_decode($order['data'],1);

        $gql = <<< EOF
            query MyQuery {
              order(id: "gid://shopify/Order/{$order->shopify_id}") {
                name
                lineItems(first: 50) {
                  edges {
                    node {
                      id
                      quantity
                      originalTotal
                      variant {
                        selectedOptions {
                          name
                          value
                        }
                      }
                      image {
                        originalSrc
                      }
                      title
                      variantTitle
                      sku
                    }
                  }
                }
                metafields(first: 50, namespace: "orders") {
                  edges {
                    node {
                      key
                      value
                    }
                  }
                }
              }
            }
EOF;

        $gqlResponse = $shop->api()->graph($gql);
        $response = [];
        if(!$gqlResponse->errors) {
            $data = $gqlResponse->body->order;
            $metafields = $gqlResponse->body->order->metafields->edges;
            if(!is_null($metafields))
                $metafields = collect($metafields)->pluck('node')->pluck('value','key')->toArray();

            foreach (@$data->lineItems->edges as $key => $val) {
                $response['lineItems'][$key] = $val->node;
                $id = str_replace('gid://shopify/LineItem/','',$val->node->id);
                $response['lineItems'][$key]->id = $id;
                $m_key = 'line_items_'.$id;
                if(array_key_exists($m_key,@$metafields)){
                    $properties = json_decode($metafields[$m_key])->properties;
                    $properties = collect($properties)->pluck('value','name');
                    $response['lineItems'][$key]->metafields = $properties;
                }
            }
            $response['name'] = $data->name;
        }
        return response($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
