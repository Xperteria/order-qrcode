<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\HelperTrait;
class TestController extends Controller
{
    use HelperTrait;
    public function index(Request $request){
        $this->createOrder(1,\ShopifyApp::shop());
    }
}
