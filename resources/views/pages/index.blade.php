<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link
        href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap"
        rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('journey_assets/css/bootstrap.min.css')  }}">
    <!-- FontAqwsome CSS File -->
    <link rel="stylesheet" href="{{ asset('journey_assets/css/font-awesome.min.css')  }}" type="text/css" />

    <link rel="stylesheet" href="{{ asset('journey_assets/css/main.css')  }}">
    <title>Product</title>
</head>

<body class="body_sm">
<header class="sticky-top bg-white">
    <nav class="navbar navbar-expand-xl c-navbar navbar-light">
        <a class="navbar-brand" href="#">
            <img src="/journey_assets/img/logo.png" alt="" class="img-fluid">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
            </ul>
        </div>
    </nav>
    <ul class="nav nav-pills nav-justified c-tab" id="pills-tab" role="tablist">
        <li class="nav-item c-tab-item">
            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
               aria-controls="pills-home" aria-selected="true">
                <span class="fa fa-user-o d-block" aria-hidden="true"></span>
                unique ID
            </a>
        </li>
        <li class="nav-item c-tab-item">
            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
               aria-controls="pills-profile" aria-selected="false">
                <span class="fa fa-user-o d-block" aria-hidden="true"></span>
                Design
            </a>
        </li>
        <li class="nav-item c-tab-item">
            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab"
               aria-controls="pills-contact" aria-selected="false">
                <span class="fa fa-user-o d-block" aria-hidden="true"></span>
                Fabric
            </a>
        </li>
        <li class="nav-item c-tab-item">
            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact-4" role="tab"
               aria-controls="pills-contact-4" aria-selected="false">
                <span class="fa fa-user-o d-block" aria-hidden="true"></span>
                Dressmakers
            </a>
        </li>
    </ul>
</header>

<section>
    <div class="container-fluid p-0">

        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                <div class="bg_fix"
                     style="background: url({{@$response->image->originalSrc}});background-repeat: no-repeat;background-attachment: fixed;background-size: cover;background-position: center top;transition: 1s;">
                    <h3>{{ @$response->product->title }}</h3>
                    <div class="scorll_sec">
                        <div class="scorll_sec_inner">
                            <h4>{{ @$response->sku }}</h4>
                            @php
                                $colours = [
                                    'nebu' => '#e9ac25',
                                    'jade' => '#607b26',
                                    'pearl' => '#f6f4f1',
                                    'hessonite' => '#f95402',
                                    'saphire' => '#2e67ae',
                                ];
                            @endphp
                            @foreach($response->selectedOptions as $key => $val)
                            <div class="row align-items-center">
                                <div class="col-6 scorll_sec_inner_info">
                                    {{ strtoupper($val->name) }}
                                </div>
                                <div class="col-6 scorll_sec_inner_detali">
                                    @if(strtolower($val->name) == "colour" || strtolower($val->name) == "color")
                                        {{ $val->value }} <span class="color_dot ml-4" style="background: {{ $colours[strtolower($val->value)] }};"></span>
                                    @elseif(strtolower($val->name) == "size")
                                        <h5 class="m-0 font-weight-normal">{{ $val->value }}</h5>
                                    @else
                                        {{ $val->value }}
                                    @endif
                                </div>
                            </div>
                            @endforeach

                            <div class="row align-items-center">
                                <div class="col-6 scorll_sec_inner_info">
                                    MADE IN
                                </div>
                                <div class="col-6 scorll_sec_inner_detali">
                                    {{$date}}
                                </div>
                            </div>
                            <a href="#" class="btn_lear_mor my-4"><span>LEARN ABOUT OUR TECH</span></a>

                            <a data-toggle="pill" aria-controls="pills-profile" href="#pills-profile"  class="btn_link_l float-right mt-3">
                                DESIGN <i class="fa fa-chevron-right ml-3" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                @php
                    $title = strtolower(@$response->product->title);
                    $image_2 = $image = "";
                    $path="/journey_assets/img/design/";
                @endphp

                    <div class="fabric_sec">
                        @if(\Str::contains($title, ['hathour']))
                            <div class="img_view_cert1">
                                <img src="{{asset($path."hathour.JPG")}}">
                            </div>
                            <p class="des_fabric_sec_b">
                                Ancient Egyptian Goddess of Women, Love and Music.
                            </p>
                            <div class="img_qut">
                                <img src="/journey_assets/img/qut.png">
                            </div>
                            <p class="des_fabric_sec mt-4">
                                Your dress is inspired by Hathour, powerful daughter of the sun God Ra and his feminine counterpart known as the Golden One whose rays illuminate the world.
                                Her stunning dress is proud and regal yet soft and elegant.
                            </p>
                            <div class="img_view_cert1">
                                <img src="{{asset($path."estatua-2.png")}}">
                            </div>
                            <p class="des_fabric_sec mt-4">
                                Featuring a majestic collar sitting delicately on the collarbone which ties with a ribbon at the nape of the neck, a pronounced waistline with a flowing skirt. Showing just a glimpse of skin, this dress captures the strength and sensuality of Hathour’s enchanting beauty.
                            </p>
                        @elseif(\Str::contains($title, ['bastet jersey']))
                            <div class="img_view_cert1">
                                <img src="{{asset($path."bastet-jersey.JPG")}}">
                            </div>
                            <p class="des_fabric_sec_b">
                                Egyptian Warrior Cat Goddess of Protection.
                            </p>
                            <div class="img_qut">
                                <img src="/journey_assets/img/qut.png">
                            </div>
                            <p class="des_fabric_sec mt-4">
                                Your dress is inspired by Bastet, who was associated with female power as she had the gentle tenderness of a (kitten) and the wild fierceness of a lioness.
                                Her dress captures feline allurity and power.

                            </p>
                            <div class="img_view_cert1">
                                <img src="{{asset($path."bastet-goddess.jpg")}}">
                            </div>
                            <p class="des_fabric_sec mt-4">
                                With adjustable fabric loops on shoulder straps for modification of the decolletage, this opulent dress has an empire waist rising about a flowing long skirt that offers you comfort and freedom of movement, flattering the female figure and capturing Bastet’s alluring felinity in a beautifully elegant design.

                            </p>
                        @elseif(\Str::contains($title, ['bastet silk']))
                            <div class="img_view_cert1">
                                <img src="{{asset($path."bastet-silk.JPG")}}">
                            </div>
                            <p class="des_fabric_sec_b">
                                Egyptian Warrior Cat Goddess of Protection.
                            </p>
                            <div class="img_qut">
                                <img src="/journey_assets/img/qut.png">
                            </div>
                            <p class="des_fabric_sec mt-4">
                                Your dress is inspired by Bastet, who was associated with female power as she had the gentle tenderness of a (kitten) and the wild fierceness of a lioness.
                                Her dress captures feline allurity and power.
                            </p>
                            <div class="img_view_cert1">
                                <img src="{{asset($path."bastet-goddess.jpg")}}">
                            </div>
                            <p class="des_fabric_sec mt-4">
                                With adjustable fabric loops on shoulder straps for modification of the decolletage, this opulent dress has an empire waist rising about a flowing long skirt that offers you comfort and freedom of movement, flattering the female figure and capturing Bastet’s alluring felinity in a beautifully elegant design.
                            </p>
                        @elseif(\Str::contains($title, ['athena silk']))
                            <div class="img_view_cert1">
                                <img src="{{asset($path."athena-silk.jpg")}}">
                            </div>
                            <p class="des_fabric_sec_b">
                                Ancient Greek Goddess of Wisdom and Justice.
                            </p>
                            <div class="img_qut">
                                <img src="/journey_assets/img/qut.png">
                            </div>
                            <p class="des_fabric_sec mt-4">
                                Your dress is inspired by Athena, daughter of Zues who sprang to life as a woman in full armour, exquisitely beautiful, with unrivaled intelligence, foresight and bravery that make her a symbol of female empowerment.
                                Her dress is regal and elegant.
                            </p>
                            <div class="img_view_cert1">
                                <img src="{{asset($path."athena-goddess.jpg")}}">
                            </div>
                            <p class="des_fabric_sec mt-4">
                                This striking one shoulder dress includes both an adjustable loop on the shoulder and a delicate ribbon belt to alter the waistline. A heavenly dress with graceful movement and fluidity that can be worn day and night, and its uniquely versatile silhouette makes you stand out at any occasion.
                            </p>
                        @elseif(\Str::contains($title, ['athena jersey']))
                            <div class="img_view_cert1">
                                <img src="{{asset($path."athena-jersey.JPG")}}">
                            </div>
                            <p class="des_fabric_sec_b">
                                Ancient Greek Goddess of Wisdom and Justice.
                            </p>
                            <div class="img_qut">
                                <img src="/journey_assets/img/qut.png">
                            </div>
                            <p class="des_fabric_sec mt-4">
                                Your dress is inspired by Athena, daughter of Zues who sprang to life as a woman in full armour, exquisitely beautiful, with unrivaled intelligence, foresight and bravery that make her a symbol of female empowerment.
                                Her dress is regal and elegant.
                            </p>
                            <div class="img_view_cert1">
                                <img src="{{asset($path."athena-goddess.jpg")}}">
                            </div>
                            <p class="des_fabric_sec mt-4">
                                Your dress is inspired by Athena, daughter of Zues who sprang to life as a woman in full armour, exquisitely beautiful, with unrivaled intelligence, foresight and bravery that make her a symbol of female empowerment.
                                Her dress is regal and elegant.
                            </p>
                        @elseif(\Str::contains($title, ['aphrodite']))
                            <div class="img_view_cert1">
                                <img src="{{asset($path."aphrodite.JPG")}}">
                            </div>
                            <p class="des_fabric_sec_b">
                                Ancient Greek Goddess
                                of Love and Passion.
                            </p>
                            <div class="img_qut">
                                <img src="/journey_assets/img/qut.png">
                            </div>
                            <p class="des_fabric_sec mt-4">
                                Your dress is inspired by Aphrodite, the alluring goddess whose divine love was so powerful, it altered the destiny of gods and mortals alike. Her timeless dress is enchanting and flows romantically around her.
                            </p>
                            <div class="img_view_cert1">
                                <img src="{{asset($path."aphrodite-goddess.jpg")}}">
                            </div>
                            <p class="des_fabric_sec mt-4">
                                This long delicate silk dress has a removable soft ribbon halter neck making it supremely flexible, comfortable and elegant and ideal for day and night. Showing just a glimpse of skin, this dress captures the romance and sensuality of Aphrodite’s ethereal beauty.
                            </p>

                        @elseif(\Str::contains($title, ['freya']))
                            <div class="img_view_cert1">
                                <img src="{{asset($path."freya.JPG")}}">
                            </div>
                            <p class="des_fabric_sec_b">
                                Ancient Norse Goddess of Beauty and Abundance.
                            </p>
                            <div class="img_qut">
                                <img src="/journey_assets/img/qut.png">
                            </div>
                            <p class="des_fabric_sec mt-4">
                                Your dress is inspired by Freya who rules over heaven, whose beauty is paralleled by her core of steel as she rides in battle with shield maidens and warriors. Her dress’s design imbues femininity and strength.
                            </p>
                            <div class="img_view_cert1">
                                <img src="{{asset($path."freya-goddess.jpg")}}">
                            </div>
                            <p class="des_fabric_sec mt-4">
                                Featuring hand braided Nordic plats which accentuate a delicate empire waist above a skirt flowing to the ground, this dress is designed to flatter all figures and channel the fierce confidence of Freya.
                            </p>
                        @endif

                        <div class="footer_btn">
                            <a data-toggle="pill" role="tab" aria-controls="pills-home-tab" href="#pills-home"  class="btn_link_l float-left">
                                <i class="fa fa-chevron-left mr-3" aria-hidden="true"></i>
                                UNIQUE ID
                            </a>
                            <a data-toggle="pill" role="tab" aria-controls="pills-contact" href="#pills-contact" class="btn_link_l float-right">
                                FABRIC <i class="fa fa-chevron-right ml-3" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>

            </div>


            <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                <div class="fabric_sec">
                    <h2 class="title">
                        YOUR FABRIC
                    </h2>
                    @php
                        $fabric = collect($response->selectedOptions)->pluck('value','name')->toArray();
                        $fabric = strtolower($fabric['Fabric']);
                        $path="/journey_assets/img/fabric/";
                    @endphp
                    @if(\Str::contains($fabric, ['jersey']))
                        <p class="des_fabric_sec">
                            At Lauren Razek, we only use sustainable fabrics and are always striving to improve our sustainability standards.
                        </p>
                        <div class="img_qut">
                            <img src="/journey_assets/img/qut.png">
                        </div>
                        <h4 class="fabric_sec_per">95% bamboo / 5% spandex</h4>
                        <div class="img_view_cert img_view_cert_b" style="background: url({{asset($path."jakob-owens-W0bOcwRJR9Y-unsplash.jpg")}});">
                        </div>
                        <h4 class="fabric_sec_per">YOUR DRESS IS:</h4>
                        <ul class="fabric_sec_ul">
                            <li>Vegan</li>
                            <li>Biodegradable</li>
                            <li>Oeko-tex certified</li>
                            <li>Organic</li>
                        </ul>
                        <p class="des_fabric_sec">
                            Bamboo jersey is spun from the bamboo plant, which grows robustly and fast without the need for much water so it doesn’t deplete the earth’s resources. The dyes use some chemicals, but the fibre is 100% biodegradable.

                            We have not yet obtained certification for this fabric, but are working on it.
                        </p>
                        <div class="img_view_cert1">
                            <img src="{{asset($path."clothes.png")}}">
                        </div>
                        <p class="des_fabric_sec">This fabric is very soft and resilient and has a slight stretch to it. It will last very long provided it is reasonably cared for. It drapes beautifully and doesn’t stick. Bamboo is an excellent choice for year-round clothing - its microporous structure and natural breathability keeps you cool and comfortable in hot weather, yet warm in the cold. </p>
                        <div class="fabric_sec_ulink">
                            <a href="#" class="under_line_link">
                                View care instructions
                            </a>
                        </div>

                    @elseif(\Str::contains($fabric, ['white silk']))
                        <p class="des_fabric_sec">
                            At Lauren Razek, we only use sustainable fabrics and are always striving to improve our sustainability standards.
                        </p>
                        <div class="img_qut">
                            <img src="/journey_assets/img/qut.png">
                        </div>
                        <h4 class="fabric_sec_per">100% Organic Bamboo Silk</h4>
                        <div class="img_view_cert img_view_cert_b" style="background: url({{asset($path."jakob-owens-W0bOcwRJR9Y-unsplash.jpg")}});">
                            <a href="#" data-toggle="modal" data-target="#exampleModalCenter"
                               class="btn_lear_mor btn_lear_mor_w my-5 mx-auto"><span>VIEW CERTIFICATES</span></a>
                        </div>
                        <h4 class="fabric_sec_per">YOUR DRESS IS:</h4>
                        <ul class="fabric_sec_ul">
                            <li>Vegan</li>
                            <li>Biodegradable</li>
                            <li>Oeko-tex certified</li>
                            <li>Organic</li>
                        </ul>
                        <p class="des_fabric_sec">
                            The fabric is woven from 100% organic bamboo yarn that is Oeko-Tex Standard 100 certified. The viscose process likely uses chemicals, but in a closed loop system and they are all removed before weaving. The finished fabric is certified by the Global Organic Textile Standard (GOTS) and by ECO-CERT.
                        </p>
                        <div class="img_view_cert1">
                            <img src="{{asset($path."clothes.png")}}">
                        </div>
                        <p class="des_fabric_sec">Bamboo silk is fabulously versatile, lightweight and flowing with a silky drape and a beautiful satin finish. Bamboo fabric is resilient and will last a long time provided it is reasonably cared for.
                        </p>
                        <p class="des_fabric_sec">  We are unable to show you the GOTS certificate at the moment as our supplier did not give us permission to share it with you, because they do not wish to reveal the source of their fabric.</p>
                        <div class="fabric_sec_ulink">
                            <a href="#" class="under_line_link">
                                View care instructions
                            </a>
                        </div>
                    @elseif(\Str::contains($fabric, ['silk']))
                        <p class="des_fabric_sec">
                            At Lauren Razek, we only use sustainable fabrics and are always striving to improve our sustainability standards.
                        </p>
                        <div class="img_qut">
                            <img src="/journey_assets/img/qut.png">
                        </div>
                        <h4 class="fabric_sec_per">100% Organic Bamboo Silk</h4>
                        <div class="img_view_cert img_view_cert_b" style="background: url({{asset($path."jakob-owens-W0bOcwRJR9Y-unsplash.jpg")}});">
                            <a href="#" data-toggle="modal" data-target="#exampleModalCenter"
                               class="btn_lear_mor btn_lear_mor_w my-5 mx-auto"><span>VIEW CERTIFICATES</span></a>
                        </div>
                        <h4 class="fabric_sec_per">YOUR DRESS IS:</h4>
                        <ul class="fabric_sec_ul">
                            <li>Vegan</li>
                            <li>Biodegradable</li>
                            <li>Oeko-tex certified</li>
                            <li>Organic</li>
                        </ul>
                        <p class="des_fabric_sec">
                            The fabric is woven from organic bamboo yarn that is Oeko-Tex Standard 100 certified. The dyes are uncertified, but all chemicals are removed before weaving. The finished fabric is also certified by ECO-CERT.
                        </p>
                        <div class="img_view_cert1">
                            <img src="{{asset($path."clothes.png")}}">
                        </div>
                        <p class="des_fabric_sec">
                            Fabric made from bamboo is more sustainable than others as the bamboo plant grows fast and robustly with little water, so it does not deplete the planet’s resources. Organic bamboo further means no pesticides or harmful chemicals were used growing the bamboo plant.
                        </p>
                        <p class="des_fabric_sec">
                            Bamboo silk is fabulously versatile, lightweight and flowing with a silky drape and a beautiful satin finish. It is resilient and will last a long time provided it is reasonably cared for.
                        </p>
                        <div class="fabric_sec_ulink">
                            <a href="#" class="under_line_link">
                                View care instructions
                            </a>
                        </div>
                    @endif


                    <div class="footer_btn">
                        <a href="#" class="btn_link_l float-left">
                            <i class="fa fa-chevron-left mr-3" aria-hidden="true"></i>
                            DESIGN
                        </a>
                        <a href="#" class="btn_link_l float-right">
                            DRESSMAKER <i class="fa fa-chevron-right ml-3" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>


            <div class="tab-pane fade" id="pills-contact-4" role="tabpanel" aria-labelledby="pills-contact-tab">
                @php
                    $fabric = collect($response->selectedOptions)->pluck('value','name')->toArray();
                    $fabric = strtolower($fabric['Fabric']);
                    $path="/journey_assets/img/dressmakers/";
                @endphp


                    <div class="fabric_sec">
                        @if(\Str::contains($fabric, ['jersey']))
                        <div class="drsmack" style="background: url({{asset($path."arts-and-crafts-close-up-colors.jpg")}});">
                            <h2 class="title">
                                WHO MADE YOUR DRESS
                            </h2>

                            <h4 class="fabric_sec_per text-left">
                                Hathour was created in our atelier by a family of Egyptian dressmakers.
                            </h4>
                            <div class="img_qut">
                                <img src="/journey_assets/img/qut.png">
                            </div>
                        </div>
                        <h4 class="fabric_sec_per fabric_sec_per1">ABDU</h4>
                        <div class="img_view_cert1 img_view_cert_drsmack">
                            <img src="{{asset($path."abdu.png")}}">
                        </div>
                        <p class="des_fabric_sec mb-3">
                            Abdu has been a dressmaker for nearly 20 years. He is the proud father of two little girls and he makes them dresses for their birthdays and for special occasions.

                            Abdu’s dream is to see his daughters grow into strong and happy women.
                        </p>
                        <h4 class="fabric_sec_per fabric_sec_per1">SHADYA</h4>
                        <div class="img_view_cert1 img_view_cert_drsmack">
                            <img src="/journey_assets/img/dessmek2.jpg">
                        </div>
                        <p class="des_fabric_sec mb-3">
                            Shadya is a part time seamstress with a keen eye for detail. </p>
                        <p class="des_fabric_sec">
                            Shadya spends her time between Cairo and Nubia in Upper Egypt where she is originally from, enjoying a
                            balance between urban and rural Egypt.
                        </p>
                        @elseif(\Str::contains($fabric, ['silk']))
                                <div class="drsmack" style="background: url({{asset($path."arts-and-crafts-close-up-colors.jpg")}});">
                                    <h2 class="title">
                                        WHO MADE YOUR DRESS
                                    </h2>

                                    <h4 class="fabric_sec_per text-left">
                                        Hathour was created in our atelier by a family of Egyptian dressmakers.
                                    </h4>
                                    <div class="img_qut">
                                        <img src="/journey_assets/img/qut.png">
                                    </div>
                                </div>
                                <h4 class="fabric_sec_per fabric_sec_per1">MOHAMMED</h4>
                                <div class="img_view_cert1 img_view_cert_drsmack">
                                    <img src="{{asset($path."MOHAMMED.png")}}">
                                </div>
                                <p class="des_fabric_sec mb-3">
                                    Mohammed was introduced to dressmaking 12 years ago by his uncle, who has taught him everything he knows. He prefers to work in the evenings and wants to learn to make bridal dresses next.

                                    Mohamed loves football and supports Liverpool because of Mo Salah.
                                </p>
                                <h4 class="fabric_sec_per fabric_sec_per1">SHADYA</h4>
                                <div class="img_view_cert1 img_view_cert_drsmack">
                                    <img src="/journey_assets/img/dessmek2.jpg">
                                </div>
                                <p class="des_fabric_sec mb-3">
                                    Shadya is a part time seamstress with a keen eye for detail. </p>
                                <p class="des_fabric_sec">
                                    Shadya spends her time between Cairo and Nubia in Upper Egypt where she is originally from, enjoying a
                                    balance between urban and rural Egypt.
                                </p>
                        @endif

                        <div class="footer_btn">
                            <a href="#" class="btn_link_l float-left">
                                <i class="fa fa-chevron-left mr-3" aria-hidden="true"></i>
                                FABRIC
                            </a>
                        </div>
                    </div>


            </div>
        </div>
    </div>
</section>

<div class="modal fade model_my" id="exampleModalCenter1" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"> &nbsp; </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    close
                </button>
            </div>
            <div class="modal-body">
                <ul class="nav c-tab c-tab_popup" id="pills-tab" role="tablist">
                    <li class="nav-item c-tab-item">
                        <a class="nav-link active" id="pills-home-tab1" data-toggle="pill" href="#pills-home1" role="tab"
                           aria-controls="pills-home1" aria-selected="true">
                            ECO-CERT
                        </a>
                    </li>
                    <li class="nav-item c-tab-item">
                        <a class="nav-link" id="pills-profile-tab1" data-toggle="pill" href="#pills-profile1" role="tab"
                           aria-controls="pills-profile1" aria-selected="false">
                            OEKO-TEX
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home1" role="tabpanel" aria-labelledby="pills-home-tab1">
                        <img class="img-fluid" src="/journey_assets/img/certificate.png">
                    </div>
                    <div class="tab-pane fade" id="pills-profile1" role="tabpanel" aria-labelledby="pills-profile-tab1">
                        <img class="img-fluid" src="/journey_assets/img/certificate.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade model_my" id="exampleModalCenter" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"> &nbsp; </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    close
                </button>
            </div>
            <div class="modal-body">
                <ul class="nav c-tab c-tab_popup c-tab_popup1" id="pills-tab" role="tablist">
                    <li class="nav-item c-tab-item">
                        <a class="nav-link active" id="pills-home-tab1" data-toggle="pill" href="#pills-home1" role="tab"
                           aria-controls="pills-home1" aria-selected="true">
                            Care Instructions
                        </a>
                    </li>
                </ul>
                <div class="qut_text mb-4">
                    <div class="qut_text1">
                        <img src="/journey_assets/img/qut.png">
                    </div>
                    <div class="qut_text2">
                        <p>We recommend you wash on a cold delicate cycle with mild detergent, do not tumble dry and iron on a cool silk setting.  </p>
                    </div>
                </div>
                <div class="care_instuct">
                    <div class="care_i">
                        <i class="fa fa-free-code-camp"></i>
                        <span>Wash at or below 30°</span>
                    </div>
                    <div class="care_i">
                        <i class="fa fa-free-code-camp"></i>
                        <span>Do not tumble dry</span>
                    </div>
                    <div class="care_i">
                        <i class="fa fa-free-code-camp"></i>
                        <span>Iron, low temp</span>
                    </div>
                    <div class="care_i">
                        <i class="fa fa-free-code-camp"></i>
                        <span>Do not bleach</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ asset('journey_assets/js/jquery-3.4.1.slim.min.js')  }}"></script>
<script src="{{ asset('journey_assets/js/popper.min.js')  }}"></script>
<script src="{{ asset('journey_assets/js/bootstrap.min.js')  }}"></script>
</body>

</html>
