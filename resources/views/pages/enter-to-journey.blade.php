<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link
        href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap"
        rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('journey_assets/css/bootstrap.min.css')  }}">
    <!-- FontAqwsome CSS File -->
    <link rel="stylesheet" href="{{ asset('journey_assets/css/font-awesome.min.css')  }}" type="text/css" />

    <link rel="stylesheet" href="{{ asset('journey_assets/css/main.css')  }}">
    <title>Demo</title>
</head>

<body class="body_sm">


<section class="welcome_sec">
    <div class="container-fluid">
        <img src="{{ asset('journey_assets/img/logo.png') }}" alt="" class="img-fluid logo_top mb-5">
        <h4>Welcome!</h4>
        <p>
            You are entitled to knowledge of the means and methods followed to create the garments you choose to wear.
        </p>
        <a href="{{ $url }}?start=1" class="round_box">
          <span>
            ENTER THE JOURNEY
          </span>
        </a>
    </div>
</section>


<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ asset('journey_assets/js/jquery-3.4.1.slim.min.js')  }}"></script>
<script src="{{ asset('journey_assets/js/popper.min.js')  }}"></script>
<script src="{{ asset('journey_assets/js/bootstrap.min.js')  }}"></script>
</body>

</html>
