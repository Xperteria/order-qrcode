@extends('layouts.app_frontend')
@section('content')
    <div class="container-fluid">
        <div class="col-md-12">

            <img src="{{$image}}" style="width:300px;">
        </div>
        <div class="col-md-12">
            <a href="{{ $url }}?page=fabric"> Fabric </a>
        </div>
    </div>
@endsection
