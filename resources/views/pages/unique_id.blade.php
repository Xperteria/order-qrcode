@extends('layouts.app_frontend')
@section('content')
    <div class="container-fluid">
        <div class="col-md-12">
            @if(!is_null($response->image))
            <img src="{{$response->image->originalSrc}}" style="width:300px;">
            @endif
            <div class="col-md-12">
                <div> {{ $response->product->title }}  </div>
                <div> {{ $response->sku }}  </div>
            </div>
                <hr/>
                <div class="col-md-12">
                    @foreach($response->selectedOptions as $key => $val)
                    <div class="col-md-12">
                            <span> {{ $val->name }}:  </span>
                            <span> {{ $val->value }}  </span>
                    </div>
                    @endforeach
                </div>
        </div>
        <hr/>
        <div class="col-md-12">
            <a href="{{ $url }}?page=design"> Design</a>
        </div>
    </div>
@endsection
