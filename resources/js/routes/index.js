import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const routes = [
    {
        path:'/',
        component: require('../components/pages/List').default,
        name:'list',
        meta: {
            title: 'Orders',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/orders-detail/:id',
        component: require('../components/pages/OrderPage').default,
        name:'order_detail',
        meta: {
            title: '',
            ignoreInMenu: 1,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
];


const router = new VueRouter({
    mode:'history',
    routes,
    // linkActiveClass:'Polaris-Tabs__Tab--selected',
    scrollBehavior() {
        return {
            x: 0,
            y: 0,
        };
    },

});
export default router;
