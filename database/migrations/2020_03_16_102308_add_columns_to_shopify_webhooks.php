<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToShopifyWebhooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shopify_webhooks', function (Blueprint $table) {
            $table->string('order_number')->nullable()->after('data');
            $table->string('customer')->nullable()->after('order_number');
            $table->string('payment_status')->nullable()->after('customer');
            $table->string('total')->nullable()->after('payment_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopify_webhooks', function (Blueprint $table) {
            $table->dropColumn(['order_number', 'customer', 'payment_status', 'total']);
        });
    }
}
