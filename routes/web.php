<?php


Route::get('/', function () {
    return view('layouts.app');
})->middleware(['auth.shop'])->name('home');


Route::group(['middleware' => 'auth.shop'], function () {
    Route::get("orders", 'Orders\OrderController@index');
    Route::get("/orders-detail/{id}", 'Orders\OrderController@show');
   

});

 Route::get("page/{shop}/{product}/{variant}", 'Page\PageController@info')->name('pages_5');
    Route::get("/test",'TestController@index');

Route::get('flush', function(){
    request()->session()->flush();
});
